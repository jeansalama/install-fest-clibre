# Install fest CLibre

Ceci est une recette d'installation d'Ubuntu pour l'install fest du 
CLibre.

## 1 - Télécharger une distribution GNU/Linux

Pour l'install fest, nous allons utiliser [Ubuntu 18.04.1 LTS](https://www.ubuntu.com/download/desktop) 
comme distribution, mais vous pouvez très bien télécharger une autre 
distribution comme elementary OS, Linux Mint, Debian, Fedora, ArchLinux 
ou encore des distributions légères comme Lubuntu, Puppy Linux, etc.

Veuillez télécharger chez vous une ou plusieurs distributions GNU/Linux 
afin d'éviter du traffic avec la connexion wifi de l'uqam (l'iso d'Ubuntu
pèse environ 2GB !!)

## 2.A - Installation de la distribution via une machine virtuelle

Ce type d'installation est simple et sécuritaire, étant donné que 
l'installation se fait virtuellement sans toucher au système 
d'exploitation actuel (il n'y a pas de risque de briser votre OS).

- Installer [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- Suivre la [video](https://www.youtube.com/watch?v=1zfO-Fhqyb8) 
  d'installation d'une machine virtuelle avec GNU/Linux

## 2.B - Installation de la distribution en mode *dual boot*

Il s'agit d'installer GNU/Linux côte à côte avec votre système 
d'exploitation actuel (windows ou Mac OS).

TODO


